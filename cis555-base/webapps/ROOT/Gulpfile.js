var gulp = require('gulp');
var server = require('tiny-lr')();
var livereload = require('gulp-livereload');
var notify = require('gulp-notify');

var paths = {
  clientScripts: ['public/js/**/*.js'],
  css: ['public/css/**/*.css'],
  html: ['*.html']
};

gulp.task('client-scripts', function () {
  return gulp.src(paths.clientScripts)
    .pipe(livereload(server))
    .pipe(notify({message: 'Scripts livereloaded.'}));
});

gulp.task('css', function () {
  gulp.src(paths.css)
    .pipe(livereload(server))
    .pipe(notify({message: 'CSS livereloaded.'}));
});

gulp.task('html', function () {
  gulp.src(paths.html)
    .pipe(livereload(server))
    .pipe(notify({message: 'HTML livereloaded.'}));
});

gulp.task('watch', function () {
  livereload.listen();
  gulp.watch(paths.clientScripts, ['client-scripts']);
  gulp.watch(paths.css, ['css']);
  gulp.watch(paths.html, ['html']);
});

gulp.task('default', ['watch']);

