_.extend(Backbone.History.prototype, {
    refresh: function() {
        this.loadUrl(this.fragment);
    }
});

var routeStripper = /^[#\/]/;
var origNavigate = Backbone.History.prototype.navigate;
Backbone.History.prototype.navigate = function (fragment, options) {
    var frag = (fragment || '').replace(routeStripper, '');
    if (this.fragment == frag)
        this.refresh();
    else
        origNavigate.call(this, fragment, options);
};