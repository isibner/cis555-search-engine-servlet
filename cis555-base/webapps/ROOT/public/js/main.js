$(function () {
  Handlebars.registerPartial('result', $('#result-template').html());
  Handlebars.registerPartial('bingResult', $('#bing-result-template').html());
  var spellCheckTemplate = Handlebars.compile($('#spell-check-template').html());
  var resultsTemplate = Handlebars.compile($('#results-template').html().split('{{&gt;').join('{{>'));


  var applyResizeOnWindowScroll = _.once(function () {
    var initialPosition = $('#preview').position();
    var initialHeight = $('#preview').height();
    var navbarHeight = $('.navbar').height();
    var onWindowScroll = function () {
      $('#preview').css({
        left: initialPosition.left,
        top: initialPosition.top + Math.max( -navbarHeight, -$(window).scrollTop()),
        height: initialHeight + Math.min(navbarHeight, $(window).scrollTop())
      });
    };
    $(window).scroll(onWindowScroll);
  });

  var SearchRouter = Backbone.Router.extend({
    routes: {
      '(:query)': 'search',   // search#kiwis
      'lmseitfy/:query': 'condescend'       // lmseitfy/kiwis
    },
    condescend: function (query) {
      $('#search-alone-wrapper').show();
      $('#after-ajax-start').hide();
      $('#search-alone-input').val('');
      var idx = 0;
      var interval = window.setInterval(function () {
        if (idx < query.length) {
          $('#search-alone-input').val($('#search-alone-input').val() + query.charAt(idx));
          idx++;
        } else {
          clearInterval(interval);
          router.navigate(query, {trigger: true});
        }
      }, 350)
    },
    search: function (query) {
      if (!query || query.trim() === '') {
        $('#search-alone-wrapper').show();
        $('#after-ajax-start').hide();
        return;
      }
      $('#search-alone-wrapper').add('#results').add('#column-container').hide();
      $('#after-ajax-start').add('#spinner').show();
      $('#navbar-search-input').val(query);
      $('#column-container').find('.spelling-suggestion').remove();

      var searchCallback = function (data) {
        if (data.bing_spellcheck[0]) {
          $('#column-container').prepend(spellCheckTemplate({suggested_spelling: data.bing_spellcheck[0].Value}));
        }
        $('#results').empty().append(resultsTemplate(data)).add('#column-container').show();
        $('#spinner').hide();
        applyResizeOnWindowScroll();
        var $frame = $('iframe');
        var newFrame = $('<iframe id="preview" src="/sneaky.html"></iframe>');
        $frame.replaceWith(newFrame);
        newFrame.show();
        $(window).trigger('scroll');
      };
      // will be $.get soon...
      $.get('/api/servlet/engine', {query: query}, searchCallback);
    }
  });
  var router = new SearchRouter();

  var doSearch = function ($input, event) {
    if (event.type === "click" || event.which === 13) {
      var query = $input.val();
      router.navigate(query, {trigger: true});
    }
  };

  var doDictate = function ($input, event) {
    if (event.type === "click") {
      var recognition = new webkitSpeechRecognition();
      recognition.onresult = function(e) {
        if (e.results.length > 0) {
          var result = e.results[0];
          if (result && result[0].transcript) {
            var text = result[0].transcript;
            $input.val(text);
            setTimeout(function () {
              router.navigate(text, {trigger: true});
            }, 500);
          }
        }
      }
      recognition.start();
    }
  }

  var searchAloneSearch = _.bind(doSearch, this, $('#search-alone-input'));
  var navbarSearch = _.bind(doSearch, this, $('#navbar-search-input'));
  var dictationAloneSearch = _.bind(doDictate, this, $('#search-alone-input'));
  var dictationNavbarSearch = _.bind(doDictate, this, $('#navbar-search-input'));

  $('#search-alone-wrapper').on('keyup', 'input', searchAloneSearch);
  $('#search-alone-wrapper').on('click', '.search-button', searchAloneSearch);
  $('#search-alone-wrapper').on('click', '.dictation-button', dictationAloneSearch);
  $('#navbar-search-wrapper').on('keyup', 'input', navbarSearch);
  $('#navbar-search-wrapper').on('click', '.search-button', navbarSearch);
  $('#navbar-search-wrapper').on('click', '.dictation-button', dictationNavbarSearch);

  $(document).on('mouseenter', '.search-result', function () {
    $(this).find('.preview-button').show();
  }).on('mouseleave', '.search-result', function () {
    $(this).find('.preview-button').hide();
  }).on('click', '.preview-button', function () {
    var link = $(this).parents('.search-result').find('.title a').attr('href');
    var $frame = $('iframe');
    var eyeOpen = $(this).hasClass('glyphicon-eye-open');
    if (eyeOpen) {
      $('.glyphicon-eye-close').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
      $(this).addClass('glyphicon-eye-close').removeClass('glyphicon-eye-open');
      var newFrame = $('<iframe id="preview" src="/wrong-permissions.html"></iframe>');
    } else {
      $('.glyphicon-eye-close').removeClass('glyphicon-eye-close').addClass('glyphicon-eye-open');
      var newFrame = $('<iframe id="preview" src="/sneaky.html"></iframe>');
    }
    $frame.replaceWith(newFrame);
    newFrame.show();
    $(window).trigger('scroll');
    if (eyeOpen) {
      newFrame.on('load', _.once(function () {
        newFrame.get(0).src = link; 
      }));
    }
  });

  Backbone.history.start();
});
