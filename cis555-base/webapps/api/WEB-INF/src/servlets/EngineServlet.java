package servlets;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.io.Charsets;
import org.apache.commons.io.FileUtils;

import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ListObjectsRequest;
import com.amazonaws.services.s3.model.ObjectListing;
import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectSummary;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.stanford.nlp.ling.CoreAnnotations.LemmaAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import utils.Bing;
import utils.Console;

public class EngineServlet extends HttpServlet {
	private static final long serialVersionUID = 4083367343671887028L;
	public static final ObjectMapper mapper = new ObjectMapper();
	private static final String delimiter = "\u2603\u2603\u2603";
	private static final String folderPath = "/home/ubuntu/aws-script/ec2-data/tfidfs_out_final_2/";
	private HashSet<String> stopWords;
	private HashMap<String, PageInfoWrapper> pageRankMap;
	private Console console;
	private AmazonS3 s3Client;
	private int totalDocs;
	// maps document to magnitude of doc vector
	private HashMap<String, Double> docVectors;
	private Properties props;
	private StanfordCoreNLP pipeline;

	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init();
		this.console = new Console();
		try {
			this.stopWords = new HashSet<String>(FileUtils.readLines(new File(
					"stopwords.txt"), Charsets.UTF_8));
			List<String> pagerankDescriptions = FileUtils.readLines(new File(
					"/home/ubuntu/aws-script/pageranks-with-descriptions"), Charsets.UTF_8);
			this.pageRankMap = new HashMap<>(4096 * 4096);
			for (String line : pagerankDescriptions) {
				String[] lineSplit = line.split(delimiter);
				if (lineSplit.length != 4) {
					if (lineSplit.length == 3) {
						try {
							String url = lineSplit[0];
							PageInfoWrapper info = new PageInfoWrapper(url,
									lineSplit[1], lineSplit[2],
									"No description available.");
							this.pageRankMap.put(hash(url), info);
						} catch (Exception e) {
							console.log(e.toString());
							console.log("ignoring");
						}
					} else {
						console.log("" + lineSplit.length);
						console.log(line);
					}
				} else {
					try {
						String url = lineSplit[0];
						PageInfoWrapper info = new PageInfoWrapper(url,
								lineSplit[1], lineSplit[2], lineSplit[3]);
						this.pageRankMap.put(hash(url), info);
					} catch (Exception e) {
						console.log(e.toString());
						console.log("ignoring");
					}
				}
			}
			this.s3Client = new AmazonS3Client(new ProfileCredentialsProvider());
			docVectors = readDocVectors();
			props = new Properties();
			props.setProperty("annotators", "tokenize, ssplit, pos, lemma");
			pipeline = new StanfordCoreNLP(props);
			/*
			S3Object object = s3Client.getObject(new GetObjectRequest(
					"cis555-bucket", "index-temp/info/-r-00000"));
			InputStream objectData = object.getObjectContent();
			BufferedReader r = new BufferedReader(new InputStreamReader(
					objectData));
			String line = r.readLine();
			if (line != null) {
				totalDocs = Integer.parseInt(line.trim());
			} else {
				totalDocs = 0;
			}*/
			System.out.println(totalDocs);
			//r.close();
		} catch (IOException e) {
			return;
		}
	}

	/**
	 * Helper method which creates a map of document hashes to weight vector
	 * magnitudes Reads from the appropriate s3 folder
	 * 
	 * @return a mapping of documents to weight vector magnitudes
	 */
	private HashMap<String, Double> readDocVectors() {
		HashMap<String, Double> result = new HashMap<String, Double>();

		ListObjectsRequest listObjectsRequest = new ListObjectsRequest()
				.withBucketName("cis555-bucket")
				.withPrefix("document-vectors/");

		ObjectListing objectListing;

		do {
			objectListing = s3Client.listObjects(listObjectsRequest);
			for (S3ObjectSummary summary : objectListing.getObjectSummaries()) {
				S3Object object = s3Client.getObject(new GetObjectRequest(
						"cis555-bucket", summary.getKey()));
				BufferedReader br = new BufferedReader(new InputStreamReader(
						object.getObjectContent()));
				String line;
				try {
					while ((line = br.readLine()) != null) {
						String[] parts = line.split("\t");

						String doc = parts[parts.length - 2];
						double sum = Double
								.parseDouble(parts[parts.length - 1]);
						result.put(doc, sum);
						totalDocs++;
					}
					br.close();
				} catch (IOException e) {
					continue;
				}
			}
		} while (objectListing.isTruncated());

		// System.out.println(result.size());
		return result;

	}

	/**
	 * Utility method for printing an object from S3. Demonstrates Java S3
	 * retrieval syntax.
	 * 
	 * @param key
	 *            The object (from our bucket) to retrieve
	 * @throws IOException
	 *             If there's an error reading the file
	 */
	public void printS3Object(String key) throws IOException {
		S3Object object = s3Client.getObject(new GetObjectRequest(
				"cis555-bucket", key));
		InputStream objectData = object.getObjectContent();
		BufferedReader r = new BufferedReader(new InputStreamReader(objectData));
		String x;
		while ((x = r.readLine()) != null) {
			console.log(x);
		}
		r.close();
	}

	/**
	 * Handle a GET request to this servlet, which should be a query which
	 * return search results
	 */
	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException {
		String query = req.getParameter("query");
		ObjectNode resultNode = mapper.createObjectNode();
		if (!req.getParameterMap().containsKey("no_bing")) {
			resultNode.set(
					"bing_search_results",
					mapper.readTree(Bing.getSearchResults(query)).get("d")
							.get("results"));
			resultNode.set(
					"bing_spellcheck",
					mapper.readTree(Bing.getSpellcheckResults(query)).get("d")
							.get("results"));
		}
		resultNode.set("results", getSearchResults(query));
		res.setContentType("application/json");
		PrintWriter writer = res.getWriter();
		mapper.writeValue(writer, resultNode);
		writer.flush();
	}

	/**
	 * Stub that demonstrates how results are expected. In JSON format, we need:
	 * [{Title: String, Url: String, Description: String}...]
	 * 
	 * @param query
	 *            The query from the user
	 * @return The JSON result.
	 */
	public static ArrayNode getFakeSearchResults(String query) {
		ArrayNode results = mapper.createArrayNode();
		for (int i = 0; i < 10; i++) {
			ObjectNode result = mapper.createObjectNode();
			result.put("Title", "Fake Data");
			result.put("Url",
					"http://www.sonypictures.com/movies/paulblartmallcop/");
			result.put(
					"Description",
					"This is fake data. This is fake data. Fake fake fake fake fake fake fake. #PaulBlart");
			results.add(result);
		}
		return results;
	}

	/**
	 * Returns real search results for a given query
	 * 
	 * @param query
	 *            The query from user
	 * @return The JSON result.
	 */
	public ArrayNode getSearchResults(String query) {
		ArrayNode results = mapper.createArrayNode();
		Annotation document = new Annotation(query);
		pipeline.annotate(document);
		List<String> newTokens = new LinkedList<String>();
		if (document != null) {
			for (CoreLabel token : document.get(TokensAnnotation.class)) {
				if (token != null) {
					String lemma = token.get(LemmaAnnotation.class);
					if (lemma != null && !lemma.isEmpty()) {
						newTokens.add(lemma.toLowerCase());
					}
				}
			}
		}

		// key: term. value -> maps each document to tfidf for that term and doc
		Map<String, TreeMap<String, Double>> termDocWeights = retrieveTermWeights(newTokens);
		Map<String, Double> queryWeights = calculateQueryWeights(newTokens,
				termDocWeights);

		ArrayList<ArrayList<String>> docLists = generateDocLists(termDocWeights);
		if (docLists.size() == 0) {
			return results;
		}
		// documents which contain all query terms
		ArrayList<String> commonDocs = intersectLists(docLists);
		// document hashes to similarities
		HashMap<String, Double> similarities = calculateSimilarities(
				commonDocs, termDocWeights, queryWeights);

		// maps ranking to list of documents associated with ranking
		// TODO: reverse the map and sort based on value?
		TreeMap<Double, ArrayList<String>> rankings = calculateRankings(similarities);
		int count = 1;
		for (Map.Entry<Double, ArrayList<String>> e : rankings.entrySet()) {
			if (count > 20) {
				break;
			}
			for (String doc : e.getValue()) {

				ObjectNode result = mapper.createObjectNode();
				if (!pageRankMap.containsKey(doc)) {
					continue;
				}
				PageInfoWrapper info = pageRankMap.get(doc);
				result.put("Title", info.title);
				result.put("Url", info.url);
				result.put("Description", info.description);
				results.add(result);
				count++;
			}
		}

		return results;
	}

	private HashMap<String, Double> normalizePageRanks(Set<String> documents) {
		double squaredSum = 0.0;
		HashMap<String, Double> result = new HashMap<String, Double>();
		for (String doc : documents) {
			if (pageRankMap.containsKey(doc)) {
				squaredSum += Math.pow(pageRankMap.get(doc).pageRank, 2);
				result.put(doc, pageRankMap.get(doc).pageRank);
			}
		}

		double magnitude = Math.sqrt(squaredSum);

		for (Map.Entry<String, Double> e : result.entrySet()) {
			result.put(e.getKey(), e.getValue() * 1.0 / magnitude);
		}
		return result;

	}

	/**
	 * Calculates the rankings of all relevant documents based on cosine
	 * similarities and pagerank
	 * 
	 * @param similarities
	 *            cosine similarities of each document
	 * @return The rankings.
	 */
	private TreeMap<Double, ArrayList<String>> calculateRankings(
			HashMap<String, Double> similarities) {
		TreeMap<Double, ArrayList<String>> rankings = new TreeMap<Double, ArrayList<String>>();
		HashMap<String, Double> normalizedPageRanks = normalizePageRanks(similarities
				.keySet());
		for (Map.Entry<String, Double> e : similarities.entrySet()) {
			double pageRank = 0;
			if (pageRankMap.containsKey(e.getKey())) {
				pageRank = normalizedPageRanks.get(e.getKey());
			}
			double rank = e.getValue() * pageRank;
			ArrayList<String> temp;
			if (!rankings.containsKey(rank)) {
				temp = new ArrayList<String>();
				temp.add(e.getKey());
			} else {
				temp = rankings.get(rank);
				temp.add(e.getKey());
			}
			// sort in descending order
			rankings.put(-1.0 * rank, temp);
		}
		return rankings;
	}

	/**
	 * Calculates the cosine similarities for a set of given documents and a
	 * given query.
	 * 
	 * @param commonDocs
	 *            documents for which to determine similarities
	 * @param termDocWeights
	 *            inverted index of tfidf values
	 * @param queryWeights
	 *            the query weight vector
	 * @return The cosine similarity for each document.
	 */
	private HashMap<String, Double> calculateSimilarities(
			ArrayList<String> commonDocs,
			Map<String, TreeMap<String, Double>> termDocWeights,
			Map<String, Double> queryWeights) {
		HashMap<String, Double> result = new HashMap<String, Double>();

		for (String doc : commonDocs) {
			if (!docVectors.containsKey(doc)) {
				continue;
			}
			double dotProduct = 0.0;
			double squaredSumQuery = 0.0;
			for (Map.Entry<String, Double> e : queryWeights.entrySet()) {
				dotProduct += e.getValue()
						* termDocWeights.get(e.getKey()).get(doc);
				squaredSumQuery += Math.pow(e.getValue(), 2);
			}

			double queryMag = Math.sqrt(squaredSumQuery);
			double docMag = docVectors.get(doc);
			result.put(doc, dotProduct / (queryMag * docMag));

		}

		return result;
	}

	/**
	 * Generates an array of the document listings for each term.
	 * 
	 * @param termDocWeights
	 *            mapping of terms to tfidf values and documents
	 * @return array of document listing
	 */
	private ArrayList<ArrayList<String>> generateDocLists(
			Map<String, TreeMap<String, Double>> termDocWeights) {
		ArrayList<ArrayList<String>> ret = new ArrayList<ArrayList<String>>();
		for (String term : termDocWeights.keySet()) {
			ArrayList<String> docList = new ArrayList<String>();
			for (Map.Entry<String, Double> entry : termDocWeights.get(term)
					.entrySet()) {
				docList.add(entry.getKey());
			}
			ret.add(docList);
		}

		return ret;
	}

	/**
	 * Find the intersection of a given list of document sets.
	 * 
	 * @param docLists
	 *            list of document lists
	 * @return intersection
	 */
	private ArrayList<String> intersectLists(
			ArrayList<ArrayList<String>> docLists) {
		ArrayList<String> compare = docLists.get(0);
		for (int i = 1; i < docLists.size(); i++) {
			compare = intersect(compare, docLists.get(i));
		}
		return compare;
	}

	/**
	 * Determines the intersection of two lists.
	 * 
	 * @param list1
	 * @param list2
	 * @return the intersection
	 */
	private ArrayList<String> intersect(ArrayList<String> list1,
			ArrayList<String> list2) {
		ArrayList<String> ret = new ArrayList<String>();
		int pos1 = 0;
		int pos2 = 0;
		while (pos1 != list1.size() && pos2 != list2.size()) {
			String docID1 = list1.get(pos1);
			String docID2 = list2.get(pos2);
			if (docID1.equals(docID2)) {
				ret.add(docID1);
				pos1++;
				pos2++;
			} else {
				if (docID1.compareTo(docID2) > 0) {
					pos2++;
				} else {
					pos1++;
				}
			}
		}
		return ret;
	}

	/**
	 * Given a query, reads all the document and associated tfidfs for each
	 * term.
	 * 
	 * @param tokens
	 *            query tokens (with stopwords removed)
	 * @return a map of terms mapped to documents and associated tfidf
	 */
	private Map<String, TreeMap<String, Double>> retrieveTermWeights(
			List<String> tokens) {
		Map<String, TreeMap<String, Double>> results = new HashMap<String, TreeMap<String, Double>>();
		for (String token : tokens) {
			try {
				String filePath = Paths.get(folderPath, hash(token)).toString();
				BufferedReader r = new BufferedReader(new InputStreamReader(
						new FileInputStream(filePath)));
				String line = r.readLine();
				r.close();
				String list = line.split("\t")[1];
				String[] documents = list.split(";");
				TreeMap<String, Double> docMap = new TreeMap<String, Double>();
				for (String doc : documents) {
					String[] parts = doc.split(":");
					String docHash = parts[0];
					int snappyIndex = docHash.indexOf(".snappy");
					try {
						String tfidfString = parts[1].split("&")[0];
						docMap.put(docHash.substring(0, snappyIndex), Double.parseDouble(tfidfString));
					} catch (ArrayIndexOutOfBoundsException e) {
						System.out.println(doc);
						continue;
					} catch (NumberFormatException e) {
						System.out.println("Ignoring doc: " + doc + " because of NumberFormatException");
						continue;
					}
				}
				results.put(token, docMap);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				continue;
			} catch (IOException e) {
				// TODO Auto-generated catch block
				continue;
			}

		}

		return results;
	}

	/**
	 * Utility method for calculating the hash of a string
	 * 
	 * @param s
	 * @return
	 */
	private String hash(String s) {
		MessageDigest sha1 = null;
		try {
			sha1 = MessageDigest.getInstance("SHA1");

		} catch (NoSuchAlgorithmException e) {
			// This really won't happen...
			e.printStackTrace();
			return null;
		}
		byte[] digest = sha1.digest(s.getBytes());
		BigInteger hash = new BigInteger(1, digest);
		return hash.toString(16);
	}

	private Map<String, Double> calculateQueryWeights(List<String> tokens,
			Map<String, TreeMap<String, Double>> termDocWeights) {
		HashMap<String, Integer> freqs = new HashMap<String, Integer>();
		for (String s : tokens) {
			if (freqs.containsKey(s)) {
				freqs.put(s, freqs.get(s) + 1);
			} else {
				freqs.put(s, 1);
			}
		}
		int maxFreq = 0;
		for (int i : freqs.values()) {
			if (i > maxFreq) {
				maxFreq = i;
			}
		}

		Map<String, Double> weights = new HashMap<String, Double>();
		for (String token : tokens) {
			if (!termDocWeights.containsKey(token)) {
				continue;
			}
			double tf = 0.5 + 0.5 * freqs.get(token) / maxFreq;
			double idf = totalDocs * 1.0 / termDocWeights.get(token).size();
			weights.put(token, tf * idf);
		}

		return weights;

	}

	private class PageInfoWrapper {
		public String url;
		public double pageRank;
		public String title;
		public String description;

		public PageInfoWrapper(String url, String rank, String title,
				String desc) {
			this.url = url;
			this.pageRank = Double.parseDouble(rank);
			if (title.length() > 100) {
				this.title = title.substring(0, 97) + "...";
			} else {
				this.title = title;
			}
			if (desc.length() > 200) {
				this.description = desc.substring(0, 197);
			} else {
				this.description = desc;
			}
		}

		public String toString() {
			return url + "\t" + this.pageRank + "\t" + this.title + "\t"
					+ this.description;
		}
	}
}
