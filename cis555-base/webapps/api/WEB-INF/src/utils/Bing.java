package utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;

public class Bing {
	public static InputStream getSpellcheckResults(String query) throws ClientProtocolException, IOException {
		String queryString = "";
		String credentialsString = "nzVRgezZ0bp0jlWvQvjRJrHHETYYRnBW3DYca6/yt0M" + ":" + "nzVRgezZ0bp0jlWvQvjRJrHHETYYRnBW3DYca6/yt0M";
		byte[] authEncBytes = Base64.encodeBase64(credentialsString.getBytes());
		String authString = "Basic " + (new String(authEncBytes));
		try {
			queryString = "?Query=%27" + URLEncoder.encode(query, "UTF-8") + "%27" + "&$format=JSON&$top=1";
		} catch (UnsupportedEncodingException e) {
			System.err.println(e.getMessage());
			return null;
		}
		return Request.Get("https://api.datamarket.azure.com/Bing/Search/v1/SpellingSuggestions" + queryString)
			.setHeader("Authorization", authString).execute().returnContent().asStream();

	}
	
	public static InputStream getSearchResults(String query) throws ClientProtocolException, IOException {
		String queryString = "";
		String credentialsString = "nzVRgezZ0bp0jlWvQvjRJrHHETYYRnBW3DYca6/yt0M" + ":" + "nzVRgezZ0bp0jlWvQvjRJrHHETYYRnBW3DYca6/yt0M";
		byte[] authEncBytes = Base64.encodeBase64(credentialsString.getBytes());
		String authString = "Basic " + (new String(authEncBytes));
		try {
			queryString = "?Query=%27" + URLEncoder.encode(query, "UTF-8") + "%27" + "&$format=JSON&$top=1";
		} catch (UnsupportedEncodingException e) {
			System.err.println(e.getMessage());
			return null;
		}
		return Request.Get("https://api.datamarket.azure.com/Bing/Search/v1/Web" + queryString)
			.setHeader("Authorization", authString).execute().returnContent().asStream();

	}
}
